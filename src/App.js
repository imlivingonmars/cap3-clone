import './App.css';

import { BrowserRouter as Router, Route, Routes, Switch } from 'react-router-dom';
import MoriNav from './components/moriNav';
import {useState, useEffect, useContext} from 'react';
import { Container } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';

//? pages
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Products from './pages/Products';
import Logout from './pages/Logout';
import AdminDashboard from './pages/AdminDashboard';
import ViewCart from './pages/viewCart';
import NotFound from './pages/Notfound';


//? context
import { UserProvider } from './UserContext';
import UserContext from './UserContext';



function App() {
    let [user, setUser] = useState({
        email: localStorage.getItem('email'),
        id: localStorage.getItem('id'),
        isAdmin: localStorage.getItem('isAdmin')
    })

    const unsetUser = () =>{
        localStorage.clear();
        //? sir's code
        /* setUser({
            id: null,
            isAdmin:null
        }) */
    }

    /* 
    useEffect(()=>{
        fetch('http://localhost:4000/users/userDetails',{
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data._id !== undefined){
                setUser({
                    email: data.email,
                    id: data._id,
                    isAdmin: data.isAdmin,
                    firstName: data.firstName
                })
            }
        })
    },[])
    */

    return(
        <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
            <MoriNav />
                <Container fluid>
                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route path="/login" element={<Login />} />
                        <Route path="/register" element={<Register />} />
                        <Route path="/products" element={<Products />} />
                        <Route path="/logout" element={<Logout/>}/>
                        <Route path="/admindashboard" element={<AdminDashboard />} />
                        <Route path="/viewCart" element={<ViewCart />} />
                        <Route path="*" element={<NotFound />} />
                    </Routes>
                </Container>
            </Router>
        </UserProvider>
    )

}


export default App;
import { Fragment } from  'react';
import Banner from '../components/Banner';
import HomeCarousel from '../components/FrontPageCarousel';


import './css/Home.css'

const data = {
    title: 'About Us',
    description: "All accessories are handmade and they're perfect for all occasions! If you're looking for cute and trendy fashion accessories, then you've come to the right shop! 😺 👂✋💍📿",
    destination: '/products',
    label: 'Shop Now'
}

export default function Home() {
    return (
        <Fragment>
        <HomeCarousel />
            <div id="homeData">
                <Banner data={data} />
                
            </div>
        </Fragment>
    )
}